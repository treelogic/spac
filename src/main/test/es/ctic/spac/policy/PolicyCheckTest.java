/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policy;

import java.net.URI;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.hp.hpl.jena.query.ResultSet;

import es.ctic.spac.appserv.SpacAppServ;
import es.ctic.spac.sparql.SparqlClient;

public class PolicyCheckTest {
	private SpacAppServ serv;
	
	@Before
	public void setUp(){
		URI endpoint = URI.create("http://wopr:8890/sparql");
		URI policiesNamegraph=URI.create("http://www.example.org/ppo");
		serv=new SpacAppServ(endpoint, endpoint, policiesNamegraph);
	}
	
	@Test
	public void testUnallowedClientSelect() throws Exception {
		URI identifier = URI.create("http://example.org#juan");
		String query = "select ?s ?o FROM <http://www.example.org/imdb> where {?s <http://www.w3.org/2000/01/rdf-schema#label> ?o} limit 1000";
		ResultSet rs = serv.executeQuery(query, SparqlClient.SPARQLJSON, identifier.toString());
		//rs.getResourceModel().write(System.out);
		Assert.assertFalse(rs.hasNext());
	}
	
	@Test
	public void testAllowedClient() throws Exception {
		URI identifier = URI.create("http://example.org#pepe");
		String query = "select ?s ?o FROM <http://www.example.org/imdb> where {?s <http://www.w3.org/2000/01/rdf-schema#label> ?o} limit 1000";
		ResultSet rs = serv.executeQuery(query, SparqlClient.SPARQLJSON, identifier.toString());
		Assert.assertTrue(rs.hasNext());
	}
	
	@Test
	public void testAllowedClientResourceAsSubject() throws Exception {
		URI identifier = URI.create("http://example.org#pepe");
		String query = "select ?p ?o FROM <http://www.example.org/imdb> where {<http://www.imdb.com/title/tt0043274> ?p ?o} limit 1000";
		ResultSet rs = serv.executeQuery(query, SparqlClient.SPARQLJSON, identifier.toString());
		Assert.assertTrue(rs.hasNext());
	}
	
	@Test
	public void testUnAllowedClientResourceAsSubject() throws Exception {
		URI identifier = URI.create("http://example.org#juan");
		String query = "select ?p ?o FROM <http://www.example.org/imdb> where {<http://www.imdb.com/title/tt0043274> ?p ?o} limit 1000";
		ResultSet rs = serv.executeQuery(query, SparqlClient.SPARQLJSON, identifier.toString());
		Assert.assertFalse(rs.hasNext());
	}

}
