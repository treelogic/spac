/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.servlet;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.ctic.spac.appserv.SpacAppServ;

public class SPACServlet extends HttpServlet implements Serializable {
	private static final long serialVersionUID = 6288401813930117053L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String endpoint = (String) request.getAttribute("endpoint");
		String policiesEndpoint = (String) request.getAttribute("policiesEndpoint");
		String query = (String) request.getAttribute("query");
		String identifier = (String) request.getAttribute("identifier");
		String format = (String) request.getAttribute("format");
		SpacAppServ serv=new SpacAppServ(URI.create(endpoint),URI.create(policiesEndpoint));//FIXME
		try {
			serv.executeQueryToOutputStream(query,format,identifier,response.getOutputStream());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
