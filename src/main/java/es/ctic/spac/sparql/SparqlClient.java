/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.sparql;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;

import org.apache.log4j.Logger;

public class SparqlClient {
	private static Logger logger = Logger.getLogger(SparqlClient.class);
	public static final String RDF = "application/rdf+xml";
	public static final String SPARQLXML = "application/sparql-results+xml";
	public static final String SPARQLJSON = "application/sparql-results+json";
	
	private String uri;
	private String format;
	
	public SparqlClient(String uri) {
		this.uri = uri;
		this.format=RDF;
	}
	
	public SparqlClient(String uri,String format) {
		this.uri = uri;
		this.format=format;
	}	

	public InputStream query(String query)
			throws MalformedURLException, UnsupportedEncodingException,
			IOException, Exception {
		URL url = this.createURL(query,format);
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		connection.setInstanceFollowRedirects(true);
		connection.setRequestProperty("Accept", format);
		if (connection.getResponseCode() == HttpURLConnection.HTTP_OK && connection.getContentType().contains(format)) {
			return connection.getInputStream();
		} else {
			logger.error("Unable to get a representation of the resource: "
					+ connection.getResponseCode() + " => "
					+ connection.getContentType());
			throw new RuntimeException(
					"Unable to get a representation of the resource");
		}
	}
	
	private URL createURL(String query,String format) throws MalformedURLException,UnsupportedEncodingException {
		MessageFormat msg = new MessageFormat("{0}?query={1}&format={2}");
        String url=msg.format(new Object[]{this.uri,URLEncoder.encode(query,"UTF-8"),URLEncoder.encode(format,"UTF-8")});
		return new java.net.URL(url);
	}
}
