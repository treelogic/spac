/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ctic.spac.model;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;

/**
 *
 * @author Ivan Minguez <ivan.minguez@fundacionctic.org>
 */
public class FileHelper {

    private static final Logger logger = Logger.getLogger(FileHelper.class);

    public static InputStream loadFileFromClasspathOrURL(String filename) throws FileNotFoundException {
        logger.debug("Opening resource input stream for filename: " + filename);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream in = classLoader.getResourceAsStream(filename);
        if (in == null) {
            try {
                in = loadOntologyFromURL(filename);
                return in;
            } catch (IOException e) {
                logger.error("Resource file not found: " + filename);
                throw new FileNotFoundException(filename);
            }
        } else {
            return in;
        }
    }

    private static InputStream loadOntologyFromURL(String url) throws IOException {
        URL uri = new URL(url);
        URLConnection connection = uri.openConnection();
        return new BufferedInputStream(connection.getInputStream());
    }
    
    
}
