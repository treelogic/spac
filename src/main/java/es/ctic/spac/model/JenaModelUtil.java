/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package es.ctic.spac.model;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.sparql.syntax.Template;
import com.hp.hpl.jena.update.UpdateAction;
import com.hp.hpl.jena.update.UpdateFactory;
import com.hp.hpl.jena.update.UpdateRequest;

import es.ctic.spac.exceptions.SparqlMethodException;
import es.ctic.spac.sparql.SparqlClient;

public class JenaModelUtil {
	private static final Logger logger = Logger.getLogger(JenaModelUtil.class);
	
    public static String joinModel(String rdf,String rdf2) throws Exception{
        Model model = ModelFactory.createDefaultModel();
        model.read(new ByteArrayInputStream(rdf.getBytes("UTF-8")),null);
        model.read(new ByteArrayInputStream(rdf2.getBytes("UTF-8")),null);
        return writeModel(model);
    }

    public static String writeModel(Model model){
        StringWriter out=new StringWriter();
        model.write(out,"RDF/XML-ABBREV");
        return out.toString();
    }

    public static Model createModel(String[] files) throws FileNotFoundException {
        Model model = ModelFactory.createDefaultModel();
        for (String url : files) {
            if(url!=null){
                model.read(FileHelper.loadFileFromClasspathOrURL(url), null);
            }
        }
        return model;
    }
    
    public static Model createModelFromInputStream(InputStream file) throws FileNotFoundException {
        Model model = ModelFactory.createDefaultModel();
        model.read(file, "");
        return model;
    }
    
    public static Model createModelFromRDF(String rdf) throws Exception{
    	Model model = ModelFactory.createDefaultModel();
        model.read(new ByteArrayInputStream(rdf.getBytes("UTF-8")),null);
        return model;
    }
    
    public static Model createEmptyModel(){
    	return ModelFactory.createDefaultModel();
    }
    
    //FIXME: Version muy preliminar
    public static String transformSelectToConstruct(String select) throws SparqlMethodException{
    	Query selectQuery = QueryFactory.create(select);
    	if(selectQuery.isConstructType()){
    		return select;
    	}else if(!selectQuery.isSelectType()){
    		throw new SparqlMethodException(); 
    	}
    	Query construct = selectQuery.cloneQuery();
    	//FIXME: Que pasa con los optional. Tendriamos que parsear la consultas
    	Template template = QueryFactory.createTemplate(selectQuery.getQueryPattern().toString());
    	construct.setConstructTemplate(template);
    	construct.setQueryConstructType();
    	return construct.toString();
    }
    
    public static Model executeSparqlQuery(String endpoint,String queryString) throws SparqlMethodException{
    	Query query = QueryFactory.create(queryString);
    	logger.debug("endpoint: "+endpoint);
		logger.debug("query: "+queryString);
    	if(query.isSelectType()){
    		query = QueryFactory.create(transformSelectToConstruct(queryString));
    		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
    		return qexec.execConstruct();
    	}else if(query.isConstructType()){
    		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
    		return qexec.execConstruct();
    	}else if(query.isDescribeType()){
    		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, query);
    		return qexec.execDescribe();
    	}else{
    		throw new SparqlMethodException();
    	}
    }
    
    public static void excuteSPARUL(Model model, String query){
        UpdateRequest request = UpdateFactory.create(query);
        UpdateAction.execute(request, model);
    }
    
    public static Model executeDescribe(Model model, String query){
    	Query select = QueryFactory.create(query);
    	QueryExecution qexec = QueryExecutionFactory.create(select, model);
    	return qexec.execDescribe();
    }
    
    public static void executeSelectToOutputStream(Model model,String query,String format,OutputStream out) throws SparqlMethodException{
    	Query select = QueryFactory.create(query);
    	QueryExecution qexec = QueryExecutionFactory.create(select, model);
    	if(select.isSelectType()){
    		ResultSet results = qexec.execSelect();
    		if(SparqlClient.SPARQLJSON.equals(format)){
    			ResultSetFormatter.outputAsJSON(out, results);
    		}else{
    			ResultSetFormatter.outputAsXML(out, results);
    		}
    	}
    	else{
    		throw new SparqlMethodException();
    	}
    }
    
    public static ResultSet executeSelect(Model model,String query,String format) throws SparqlMethodException{
    	Query select = QueryFactory.create(query);
    	QueryExecution qexec = QueryExecutionFactory.create(select, model);
    	if(select.isSelectType()){
    		return qexec.execSelect();
    	}
    	else{
    		throw new SparqlMethodException();
    	}
    }
    
	public static boolean executeAsk(String endpoint,String query){
		return executeAsk(endpoint, null, query);
	}
	
	public static boolean executeAsk(String endpoint,String namedgraph,String query){
		query="PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"+query; //FIXME
		Query select = QueryFactory.create(query);
		if(namedgraph!=null){
			select.addGraphURI(namedgraph);
		}
		logger.debug("Query: "+select.toString());
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, select);
    	return qexec.execAsk();
	}
	
	public static ResultSet executeSelect(String endpoint,String query){
		return executeSelect(endpoint, null, query);
	}
	
	public static ResultSet executeSelect(String endpoint,String namedgraph,String query){
		Query select = QueryFactory.create(query);
		if(namedgraph!=null){
			select.addGraphURI(namedgraph);
		}
		logger.debug("Query: "+select.toString());
		QueryExecution qexec = QueryExecutionFactory.sparqlService(endpoint, select);
    	return qexec.execSelect();
	}
}
