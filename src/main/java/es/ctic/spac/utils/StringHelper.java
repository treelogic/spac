/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package es.ctic.spac.utils;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Ivan Minguez <ivan.minguez@fundacionctic.org>
 */
public class StringHelper {
    public static String getContent(InputStream in) throws IOException {
        StringBuffer out = new StringBuffer();
        byte[] b = new byte[in.available()];
        in.read(b);
        out.append(new String(b, 0, b.length));
        return out.toString();
    }
}
