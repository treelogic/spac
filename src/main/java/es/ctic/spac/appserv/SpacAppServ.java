/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.appserv;

import java.io.OutputStream;
import java.net.URI;
import java.util.List;

import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;

import es.ctic.spac.filter.JenaModelFilterVisitor;
import es.ctic.spac.model.JenaModelUtil;
import es.ctic.spac.policies.loader.PoliciesLoader;
import es.ctic.spac.policies.loader.SparqlPoliciesLoader;
import es.ctic.spac.policies.model.Policy;


public class SpacAppServ {
	private URI endpoint;
	private URI policiesEndpoint;
	private URI policiesNamedGraph;
	
	public SpacAppServ(URI endpoint,URI policiesEndpoint){
		this.endpoint=endpoint;
		this.policiesEndpoint=policiesEndpoint;
	}
	
	public SpacAppServ(URI endpoint,URI policiesEndpoint,URI policiesNamegraph){
		this(endpoint,policiesEndpoint);
		this.policiesNamedGraph=policiesNamegraph;
	}	
	
	public ResultSet executeQuery(String query,String format,String identifier) throws Exception{
		Model model=executeQueryFromEndpoint(endpoint.toString(), query, identifier);
		ResultSet resultSet = JenaModelUtil.executeSelect(model, query, format);
		return resultSet;
	}
	
	public Model executeDescribe(String query,String identifier) throws Exception{
		Model model=executeQueryFromEndpoint(endpoint.toString(), query, identifier);
		return JenaModelUtil.executeDescribe(model, query);
	}

	public void executeQueryToOutputStream(String query,String format,String identifier,OutputStream out) throws Exception{
		Model model=executeQueryFromEndpoint(endpoint.toString(), query, identifier);
		JenaModelUtil.executeSelectToOutputStream(model, query,format,out);
	}
	
	private Model executeQueryFromEndpoint(String endpoint, String query, String identifier) throws Exception{
		Model model=JenaModelUtil.executeSparqlQuery(endpoint, query);
		JenaModelFilterVisitor policiesVisitor=new JenaModelFilterVisitor(model, URI.create(identifier), policiesEndpoint, policiesNamedGraph);
		List<Policy> policies=loadPolicies();
		for(Policy policy:policies){
			policiesVisitor.visit(policy);
		}
		return model;
	}
	
	private List<Policy> loadPolicies(){
		PoliciesLoader loader=new SparqlPoliciesLoader(policiesEndpoint, policiesNamedGraph);
		return loader.loadPolicies();
	}

}
