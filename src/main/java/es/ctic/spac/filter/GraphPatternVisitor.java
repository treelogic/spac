/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.filter;

import es.ctic.spac.policies.model.ClassAsObjectPolicy;
import es.ctic.spac.policies.model.ClassAsSubjectPolicy;
import es.ctic.spac.policies.model.HasLiteralPolicy;
import es.ctic.spac.policies.model.HasPropertyPolicy;
import es.ctic.spac.policies.model.ResourceAsObjectPolicy;
import es.ctic.spac.policies.model.ResourceAsSubjectPolicy;

public class GraphPatternVisitor extends BasicVisitor{
	private String subject;
	private String property;
	private String object;
	
	public GraphPatternVisitor(){
		this.subject="?s";
		this.property="?p";
		this.object="?o";
	}
	
	public String getPattern(){
		return subject+" "+property+" "+object;
	}
	
	public Object visit(ResourceAsSubjectPolicy policy){
		subject="<"+policy.getResource().toString()+">";
		return null;
	}
	
	public Object visit(ResourceAsObjectPolicy policy){
		object="<"+policy.getResource().toString()+">";
		return null;
	}
	
	public Object visit(ClassAsSubjectPolicy policy){
		return null;
	}
	
	public Object visit(ClassAsObjectPolicy policy){
		return null;
	}
	
	public Object visit(HasPropertyPolicy policy){
		property="<"+policy.getProperty().toString()+">";
		return null;
	}
	
	public Object visit(HasLiteralPolicy policy){
		object="\""+policy.getLiteral()+"\"";
		return null;
	}
    
}
