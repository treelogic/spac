/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.filter;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class BasicVisitor {
    
	@SuppressWarnings({ "unused", "rawtypes" })
	private Class nodesType;
    
    public BasicVisitor() {
    	 nodesType = null;
    }
    
    public BasicVisitor(String nodesTypeName) throws ClassNotFoundException {
    	this.nodesType = Class.forName(nodesTypeName);
    }
    
	public BasicVisitor(@SuppressWarnings("rawtypes") Class nodesType) {
    	this.nodesType = nodesType;
    }
    
    public Object visit(Object node) throws Exception{
    	return visit("visit", node);
    }
    
	public Object visit(@SuppressWarnings("rawtypes") ArrayList list) throws Exception{
    	return visit("visit", list);
    }
    
    public Object visit(String methodName, Object node) throws Exception{
    	System.out.println(node.getClass()+ " - "+methodName);
    	try {
    		Method method = getClass().getMethod(methodName, new Class[] { node.getClass() });
    		Object invoke = method.invoke(this, new Object[] { node });
    		return invoke;
    	} catch (Exception e) {
    		throw new Exception(e);
    	}
    }  
}