/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.filter;

import java.net.URI;
import java.text.MessageFormat;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.rdf.model.Model;

import es.ctic.spac.model.JenaModelUtil;
import es.ctic.spac.policies.checker.AskPolicyChecker;
import es.ctic.spac.policies.checker.PolicyChecker;
import es.ctic.spac.policies.model.ClassAsObjectPolicy;
import es.ctic.spac.policies.model.ClassAsSubjectPolicy;
import es.ctic.spac.policies.model.ComplexPolicy;
import es.ctic.spac.policies.model.HasLiteralPolicy;
import es.ctic.spac.policies.model.HasPropertyPolicy;
import es.ctic.spac.policies.model.Policy;
import es.ctic.spac.policies.model.ResourceAsObjectPolicy;
import es.ctic.spac.policies.model.ResourceAsSubjectPolicy;

public class JenaModelFilterVisitor extends BasicVisitor{
	private static final Logger logger = Logger.getLogger(JenaModelFilterVisitor.class);
	
	private PolicyChecker checker;
	private Model model;
	private URI client;
	
	public JenaModelFilterVisitor(Model model,URI client,URI policyEndpoint,URI namedgraph){
		this.model=model;
		this.client=client;
		String graph=namedgraph==null?null:namedgraph.toString();
		this.checker=new AskPolicyChecker(policyEndpoint.toString(),graph);
		logger.debug("Policies Endpoint:"+policyEndpoint);
		logger.debug("Policies GraphName: "+namedgraph);
	}
	
	public Object visit(HasPropertyPolicy policy){
		logger.debug("Loading HasProperty");
		if(!checker.check(policy,client)){
			deleteProperty(model, policy.getProperty());
		}
		return null;
	}
	
	public Object visit(ResourceAsSubjectPolicy policy){
		logger.debug("Loading ResourceAsSubject");
		if(!checker.check(policy,client)){
			deleteResourceAsSubject(model, policy.getResource());
		}
		return null;
	}
	
	public Object visit(ResourceAsObjectPolicy policy){
		logger.debug("Loading ResourceAsObject");
		if(!checker.check(policy,client)){
			deleteResourceAsObject(model, policy.getResource());
		}
		return null;
	}
	
	public Object visit(ClassAsSubjectPolicy policy){
		logger.debug("Loading ClassAsSubject");
		if(!checker.check(policy,client)){
			deleteClassAsSubject(model, policy.getOwlClass());
		}
		return null;
	}
	
	public Object visit(ClassAsObjectPolicy policy){
		logger.debug("Loading ClassAsObject");
		if(!checker.check(policy,client)){
			deleteClassAsObject(model, policy.getOwlClass());
		}
		return null;
	}
	
	public Object visit(HasLiteralPolicy policy){
		logger.debug("Loading HasLiteral");
		if(!checker.check(policy,client)){
			hasLiteral(model, policy.getLiteral());
		}
		return null;
	}
	
	public Object visit(ComplexPolicy policy){
		logger.debug("Loading ComplexPolicy");
		if(!checker.check(policy,client)){
			deleteComplexGraph(model, policy);
		}
		return null;
	}
	
    private void deleteComplexGraph(Model model2, ComplexPolicy policy) {
    	StringBuffer patterns=new StringBuffer();
    	GraphPatternVisitor gpv=new GraphPatternVisitor();
    	for(Policy part:policy.getPolicies()){
    		try {
				String pattern=(String)gpv.visit(part);
				patterns.append(pattern).append(" . \n");
			} catch (Exception e) {
				e.printStackTrace();
			}
    	}
    	StringBuffer sb=new StringBuffer();
    	String query=sb.append("DELETE {\n").append(gpv.getPattern()).append(" WHERE {").append(gpv.getPattern()).append("}").toString();
        JenaModelUtil.excuteSPARUL(model, query);
	}

	private void deleteProperty(Model model, URI property){
    	MessageFormat format = new MessageFormat("DELETE '{'?s <{0}> ?o'}' WHERE '{'?s <{0}> ?o'}'");
        String delete=format.format(new Object[]{ property.toString()});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
    
    private void deleteResourceAsSubject(Model model, URI resource){
    	MessageFormat format = new MessageFormat("DELETE '{'<{0}> ?p ?o'}' WHERE '{'<{0}> ?p ?o'}'");
        String delete=format.format(new Object[]{ resource.toString()});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
    
    private void deleteResourceAsObject(Model model, URI resource){
    	MessageFormat format = new MessageFormat("DELETE '{'?s ?p <{0}>'}' WHERE '{'?s ?p <{0}>'}'");
        String delete=format.format(new Object[]{ resource.toString()});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
    
    private void deleteClassAsSubject(Model model, URI resource){
    	MessageFormat format = new MessageFormat("DELETE '{'<{0}> ?p ?o'}' WHERE '{'<{0}> ?p ?o . "+
    			"<{0}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class>'}'");
        String delete=format.format(new Object[]{ resource.toString()});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
    
    private void deleteClassAsObject(Model model, URI resource){
    	MessageFormat format = new MessageFormat("DELETE '{'?s ?p <{0}>}' WHERE '{'?s ?p <{0}> . "+
    			"<{0}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2002/07/owl#Class>'}'");
        String delete=format.format(new Object[]{ resource.toString()});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
    
    private void hasLiteral(Model model, String literal){
    	MessageFormat format = new MessageFormat("DELETE '{'?s ?p ?o}' WHERE '{'?s ?p ?o. FILTER (?o == \"{0}\") '}'");
        String delete=format.format(new Object[]{ literal});
        JenaModelUtil.excuteSPARUL(model, delete);
    }
}
