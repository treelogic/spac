/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.checker;

import java.net.URI;

import org.apache.log4j.Logger;

import es.ctic.spac.model.JenaModelUtil;
import es.ctic.spac.policies.model.AskPolicy;


public class AskPolicyChecker implements PolicyChecker{
	private static final Logger logger = Logger.getLogger(AskPolicyChecker.class);
	
	private String endpoint;
	private String namedgraph;
	
	public AskPolicyChecker(String endpoint,String namedgraph){
		this.endpoint=endpoint;
		this.namedgraph=namedgraph;
	}
	
	@Override
	public boolean check(AskPolicy policy,URI client){
		for(String ask:policy.getAskQueries()){
			String query=ask.replaceAll("\\?x", "<"+client.toString()+">");
			logger.info("Checking ASK condition");
			logger.debug("Query: "+query);
			if(JenaModelUtil.executeAsk(endpoint, namedgraph, query)){
				logger.debug("ASK result: true");
				return true;
			}
		}
		logger.debug("ASK result: false");
		return false;
	}
}
