/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.model;

import java.net.URI;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class AskPolicy extends Policy{
	private List<String> askQueries;
	
	public AskPolicy(URI uri){
		super(uri);
		askQueries=new LinkedList<String>();
	}
	
	public List<String> getAskQueries(){
		return Collections.unmodifiableList(askQueries);
	}
	
	public void addAskQuery(String ask){
		askQueries.add(ask);
	}
	
	public void addAskQueries(List<String> asks){
		askQueries.addAll(asks);
	}
}
