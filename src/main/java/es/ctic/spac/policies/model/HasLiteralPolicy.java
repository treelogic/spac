/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.model;

import java.net.URI;

public class HasLiteralPolicy extends AskPolicy{
	private String literal;
	
	public HasLiteralPolicy(URI uri,String literal){
		super(uri);
		this.literal=literal;
	}
	
	public String getLiteral(){
		return literal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.literal == null) ? 0 : this.literal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HasLiteralPolicy other = (HasLiteralPolicy) obj;
		if (this.literal == null) {
			if (other.literal != null)
				return false;
		} else if (!this.literal.equals(other.literal))
			return false;
		return true;
	}
	
	
}
