/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.model;

import java.net.URI;

public class ClassAsObjectPolicy extends AskPolicy{
	private URI _class;
	
	public ClassAsObjectPolicy(URI uri,URI _class){
		super(uri);
		this._class=_class;
	}
	
	public URI getOwlClass(){
		return _class;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this._class == null) ? 0 : this._class.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassAsObjectPolicy other = (ClassAsObjectPolicy) obj;
		if (this._class == null) {
			if (other._class != null)
				return false;
		} else if (!this._class.equals(other._class))
			return false;
		return true;
	}
	
	
}
