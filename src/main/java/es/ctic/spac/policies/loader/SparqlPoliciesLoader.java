/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;

import es.ctic.spac.model.FileHelper;
import es.ctic.spac.model.JenaModelUtil;
import es.ctic.spac.policies.model.AskPolicy;
import es.ctic.spac.policies.model.ClassAsObjectPolicy;
import es.ctic.spac.policies.model.ClassAsSubjectPolicy;
import es.ctic.spac.policies.model.HasLiteralPolicy;
import es.ctic.spac.policies.model.HasPropertyPolicy;
import es.ctic.spac.policies.model.Policy;
import es.ctic.spac.policies.model.ResourceAsObjectPolicy;
import es.ctic.spac.policies.model.ResourceAsSubjectPolicy;
import es.ctic.spac.utils.StringHelper;

public class SparqlPoliciesLoader extends PoliciesLoader{
	private static final Logger logger = Logger.getLogger(SparqlPoliciesLoader.class);
	
	private URI endpoint;
	private URI namedgraph;
	private String template;

	
	public SparqlPoliciesLoader(URI endpoint, URI namedgraph){
		this.endpoint=endpoint;
		this.namedgraph=namedgraph;
		try {
			InputStream in = FileHelper.loadFileFromClasspathOrURL("policyQueryTemplate.txt");
			template=StringHelper.getContent(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Policy> loadPolicies(){
		List<Policy> policies=new LinkedList<Policy>();
		policies.addAll(loadResourceAsSubject());
		policies.addAll(loadResourceAsObject());
		policies.addAll(loadClassAsSubject());
		policies.addAll(loadClassAsObject());
		policies.addAll(loadHasProperty());
		policies.addAll(loadHasLiteral());
		return policies;
	}

	private List<Policy> loadResourceAsSubject() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("resourceAsSubject");
		while(rs.hasNext()){
			logger.info("Loaded resourceAsSubject policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Resource property=solution.getResource("element");
			Literal ask=solution.getLiteral("ask");
			AskPolicy policy=new ResourceAsSubjectPolicy(URI.create(uri.getURI()),URI.create(property.getURI()));
			logger.debug("Finded ask query: "+ ask.getString());
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}

	private List<Policy> loadResourceAsObject() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("resourceAsObject");
		while(rs.hasNext()){
			logger.info("Loaded resourceAsObject policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Resource property=solution.getResource("element");
			Literal ask=solution.getLiteral("ask");
			AskPolicy policy=new ResourceAsObjectPolicy(URI.create(uri.getURI()),URI.create(property.getURI()));
			logger.debug("Finded ask query: "+ ask.getString());
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}


	private List<Policy> loadClassAsSubject() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("classAsSubject");
		while(rs.hasNext()){
			logger.info("Loaded classAsSubject policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Resource property=solution.getResource("element");
			Literal ask=solution.getLiteral("ask");
			AskPolicy policy=new ClassAsSubjectPolicy(URI.create(uri.getURI()),URI.create(property.getURI()));
			logger.debug("Finded ask query: "+ ask.getString());
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}

	private List<Policy> loadClassAsObject() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("classAsObject");
		while(rs.hasNext()){
			logger.info("Loaded classAsObject policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Resource property=solution.getResource("element");
			Literal ask=solution.getLiteral("ask");
			AskPolicy policy=new ClassAsObjectPolicy(URI.create(uri.getURI()),URI.create(property.getURI()));
			logger.debug("Finded ask query: "+ ask.getString());
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}

	private List<Policy> loadHasProperty() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("hasProperty");
		while(rs.hasNext()){
			logger.info("Loaded hasProperty policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Resource property=solution.getResource("element");
			Literal ask=solution.getLiteral("ask");
			HasPropertyPolicy policy=new HasPropertyPolicy(URI.create(uri.getURI()),URI.create(property.getURI()));
			logger.debug("Policy "+uri+" found ask query: "+ ask.getString()+ " for property "+property);
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}
	
	private ResultSet searchPolicies(String propertyName){
		String query=template.replaceFirst("%policyType%", propertyName);
		String graph=namedgraph==null?null:namedgraph.toString();
		ResultSet rs=JenaModelUtil.executeSelect(endpoint.toString(),graph, query.toString());
		return rs;
	}

	private List<Policy> loadHasLiteral() {
		List<Policy> policies=new LinkedList<Policy>();
		ResultSet rs=searchPolicies("hasLiteral");
		while(rs.hasNext()){
			logger.info("Loaded hasLiteral policy");
			QuerySolution solution = rs.next();
			Resource uri=solution.getResource("policy");
			Literal literal=solution.getLiteral("element");
			Literal ask=solution.getLiteral("ask");
			HasLiteralPolicy policy=new HasLiteralPolicy(URI.create(uri.getURI()),literal.toString());
			logger.debug("Finded ask query: "+ ask.getString());
			policy.addAskQuery(ask.getString());
			policies.add(policy);
		}
		return policies;
	}

	
}
