/**
 * (c) Copyright Treelogic, 2011
 * http://www.treelogic.com/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ctic.spac.policies.loader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import es.ctic.spac.model.FileHelper;
import es.ctic.spac.model.JenaModelUtil;
import es.ctic.spac.policies.model.AskPolicy;
import es.ctic.spac.policies.model.ClassAsObjectPolicy;
import es.ctic.spac.policies.model.ClassAsSubjectPolicy;
import es.ctic.spac.policies.model.ComplexPolicy;
import es.ctic.spac.policies.model.HasLiteralPolicy;
import es.ctic.spac.policies.model.HasPropertyPolicy;
import es.ctic.spac.policies.model.Policy;
import es.ctic.spac.policies.model.ResourceAsObjectPolicy;
import es.ctic.spac.policies.model.ResourceAsSubjectPolicy;
import es.ctic.spac.utils.StringHelper;

public class SparqlComplexPoliciesLoader extends PoliciesLoader{
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(SparqlComplexPoliciesLoader.class);
	
	private URI endpoint;
	private URI namedgraph;
	private String templatePolicies;
	private String templateAsks;
	
	public SparqlComplexPoliciesLoader(URI endpoint, URI namedgraph){
		this.endpoint=endpoint;
		this.namedgraph=namedgraph;
		try {
			InputStream in = FileHelper.loadFileFromClasspathOrURL("policyComplexQueryTemplate.txt");
			templatePolicies=StringHelper.getContent(in);
			InputStream in2 = FileHelper.loadFileFromClasspathOrURL("policyASKTemplate.txt");
			templatePolicies=StringHelper.getContent(in2);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public List<Policy> loadPolicies(){
		List<Policy> p=new LinkedList<Policy>();
		ResultSet rs=searchPolicies();
		Map<String,List<Pair>> policies=parseResults(rs);
		Set<String> keys = policies.keySet();
		Iterator<String> iterator = keys.iterator();
		while(iterator.hasNext()){
			String key=iterator.next();
			List<Pair> pairs=policies.get(key);
			AskPolicy policy=null;
			if(pairs.size()>1){
				policy=new ComplexPolicy(URI.create(key));
				for(Pair pair:pairs){
					((ComplexPolicy)policy).getPolicies().add(createPolicy(URI.create(key),pair));
				}
			}else{
				policy=createPolicy(URI.create(key),pairs.get(0));
			}
			policy.addAskQueries(loadASKConditions(policy.getUri()));
			p.add(policy);
		}
		return null;
	}
	
	private List<String> loadASKConditions(URI uri) {
		List<String> asks=new LinkedList<String>();
		String query=templateAsks.replaceAll("%policy%", uri.toString());
		ResultSet rs=JenaModelUtil.executeSelect(endpoint.toString(),namedgraph.toString(), query);
		while(rs.hasNext()){
			QuerySolution next = rs.nextSolution();
			asks.add(next.getLiteral("ask").getString());
		}
		return asks;
	}

	private AskPolicy createPolicy(URI uri,Pair pair) {
		if("http://vocab.deri.ie/ppo#resourceAsSubject".equals(pair.type)){
			return new ResourceAsSubjectPolicy(uri, URI.create(pair.element));
		}else if("http://vocab.deri.ie/ppo#resourceAsObject".equals(pair.type)){
			return new ResourceAsObjectPolicy(uri, URI.create(pair.element));
		}else if("http://vocab.deri.ie/ppo#classAsSubject".equals(pair.type)){
			return new ClassAsSubjectPolicy(uri, URI.create(pair.element));
		}else if("http://vocab.deri.ie/ppo#classAsObject".equals(pair.type)){
			return new ClassAsObjectPolicy(uri, URI.create(pair.element));
		}else if("http://vocab.deri.ie/ppo#hasProperty".equals(pair.type)){
			return new HasPropertyPolicy(uri, URI.create(pair.element));
		}else if("http://vocab.deri.ie/ppo#hasLiteral".equals(pair.type)){
			return new HasLiteralPolicy(uri, pair.element);
		}
		return null;
	}

	private Map<String,List<Pair>> parseResults(ResultSet rs){
		Map<String,List<Pair>> policies=new HashMap<String, List<Pair>>();
		while(rs.hasNext()){
			QuerySolution next = rs.next();
			String uri=next.getResource("policy").getURI();
			List<Pair> pairs;
			if(policies.containsKey(uri)){
				pairs=policies.get(uri);
			}else{
				pairs=new LinkedList<SparqlComplexPoliciesLoader.Pair>();
			}
			String type=next.getResource("type").getURI();
			String element=null;
			if(next.get("element").isLiteral()){
				element=next.getLiteral("element").getString();
			}else{
				element=next.getResource("element").getURI();
			}
			pairs.add(new Pair(type, element));
		}
		return policies;
	}
	
	private ResultSet searchPolicies(){
		ResultSet rs=JenaModelUtil.executeSelect(endpoint.toString(),namedgraph.toString(), templatePolicies);
		return rs;
	}
	
		
	class Pair{
		protected String type;
		protected String element;
		
		public Pair(String type,String element){
			this.type=type;
			this.element=element;
		}
	}
}
