SPAC (Sparql Proxy for Access Control)
---------------------------------------

SPAC is a proxy that can be deoployed on top of any SPARQL endpoint, allowing to filter the results of queries performed according the access control policies defined. SPAC (partially) applies policies defined the PPO ontology developed by DERI. 

This ontology, extending Web Access Control (WAC) ontology, provides a set of axioms to define the conditions necessary for accessing some part of a RDF graph. Further details at http://vocab.deri.ie/ppo

Implementation status:
---------------------

Currently SPAC implements some part of PPO:

	* ppo:resourceAsSubject
	* ppo:resourceAsObject
	* ppo:classAsSubject
	* ppo:classAsObject
	* ppo:hasLiteral
	* ppo:hasProperty

Sample of access control policy using PPO:
-----------------------------------------

This sample policy asserts that only the people with the rol "CTICworkmate" can access my phone number:

	ex:policy-example a  ppo:PrivacyPreference ;
		ppo:hasCondition 
		[
		   ppo:hasProperty  foaf:phone
		];
		ppo:hasAccessSpace
		[ 
		  ppo:hasAccessQuery   “ASK {?x  wai:plays ex:CTICworkmate}”
		] .


Usage examples:
---------------

The library can be used as follows:

	URI endpoint;
	URI policiesEndpoint;
	String query="SELECT {?s ?p ?o} WHERE {?s ?p ?o}";
	String format="application/json";
	String identifier="http://uri-del-perfil-que-accede/";
	SpacAppServ serv=new SpacAppServ(endpoint, policiesEndpoint);
	ResultSet results=serv.executeQuery(query, format, identifier);

In addition, the SPACServlet provides a serlet-based implementation of the SPARQL access control proxy, that can be customized bt using the following set of parameters:

	* request["endpoint"]: SPARQL endpoint to query
	* request["policiesEndpoint"]: SPARQL endpoint to query where retrieve the policies
	* request["query"]: SPARQL query to perform (compatible with the SPARQL Protocol)
	* request["identifier"]: URI with the profile with the identity (WebID must deprecated this)
	* request["format"]: Result format (if empty, content negotiation is performed)

